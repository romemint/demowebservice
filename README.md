# Project Name

DemoWebService

## Installation

1. clone project on directory `git clone https://romemint@bitbucket.org/romemint/demowebservice.git`
2. import project om ide eclipse or sts4.
3. update maven project.
4. create database name <database-name> on mysql.
5. run file `script.sql` file.
6. config database on `application.properties` file by <database-name> etc.
7. run the `DemoWebServiceApplication.java` as a Java application.
8. import `demo.postman_collection.json` file for test rest api. 