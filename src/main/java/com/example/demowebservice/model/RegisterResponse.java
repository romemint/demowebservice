package com.example.demowebservice.model;

import java.io.Serializable;

public class RegisterResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String status;
	String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
