package com.example.demowebservice.model;

public enum MemberType {
    PLATINUM, GOLD, SILVER;
}