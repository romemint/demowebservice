package com.example.demowebservice.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.example.demowebservice.model.JwtRequest;
import com.example.demowebservice.model.RegisterRequest;
import com.example.demowebservice.model.RegisterResponse;

public interface AuthenticationService {
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
	
	public UserDetails loadUserByUsernameAndPassword(JwtRequest jwtRequest) throws UsernameNotFoundException;
	
	public RegisterResponse register(RegisterRequest registerRequest) throws Exception;
}