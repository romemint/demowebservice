package com.example.demowebservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demowebservice.entity.Users;
import com.example.demowebservice.model.JwtRequest;
import com.example.demowebservice.model.MemberType;
import com.example.demowebservice.model.RegisterRequest;
import com.example.demowebservice.model.RegisterResponse;
import com.example.demowebservice.respository.UsersRespository;
import com.example.demowebservice.service.AuthenticationService;
import com.example.demowebservice.util.SaltedMD5;
import com.example.demowebservice.util.UpdatableBCrypt;

@Service
public class AuthenticationServiceImpl implements AuthenticationService, UserDetailsService {

	@Autowired
	private UsersRespository usersRespository;

	@Override
	public UserDetails loadUserByUsernameAndPassword(JwtRequest jwtRequest) throws UsernameNotFoundException {
		String usernane = jwtRequest.getUsername();
		String password = SaltedMD5.encrypt(jwtRequest.getPassword());
		List<Users> res = usersRespository.findByUsernameAndPassword(usernane, password);
		if (res.size() > 0) {
			UpdatableBCrypt bcrypt = new UpdatableBCrypt(10);
			return new User(usernane, bcrypt.hash(password),
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + usernane);
		}
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<Users> res = usersRespository.findByUsername(username);
		if (res.size() > 0) {
			UpdatableBCrypt bcrypt = new UpdatableBCrypt(10);
			return new User(res.get(0).getUsername(), bcrypt.hash(res.get(0).getPassword()),
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

	@Override
	public RegisterResponse register(RegisterRequest registerRequest) throws Exception {

		Users users = new Users();
		users.setAddress(registerRequest.getAddress());
		users.setPassword(SaltedMD5.encrypt(registerRequest.getPassword()));
		users.setPhone(registerRequest.getPhone());
		users.setUsername(registerRequest.getUsername());
		Double salary = registerRequest.getSalary();
		if(salary > 50000) {
			users.setMemberType(MemberType.PLATINUM);
		}else if(salary >= 30000) {
			users.setMemberType(MemberType.GOLD);
		}else {
			users.setMemberType(MemberType.SILVER);
		}
		users.setReferenceCode(genReferenceCode(registerRequest.getPhone()));
		users.setSalary(salary);
		usersRespository.save(users);

		RegisterResponse registerResponse = new RegisterResponse();
		registerResponse.setMessage("");
		registerResponse.setStatus("success");
		return registerResponse;
	}

	public String genReferenceCode(String phone) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
		String str = dateFormat.format(date);
		str += phone.substring(phone.length() - 4);
		return str;
	}
}
