package com.example.demowebservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demowebservice.entity.Users;
import com.example.demowebservice.respository.UsersRespository;
import com.example.demowebservice.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UsersRespository usersRespository;

	@Override
	public List<Users> getUsers() throws UsernameNotFoundException {
		return usersRespository.findAll();
	}
}
