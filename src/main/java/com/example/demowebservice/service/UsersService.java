package com.example.demowebservice.service;

import java.util.List;

import com.example.demowebservice.entity.Users;

public interface UsersService {
	public List<Users> getUsers();
}