package com.example.demowebservice.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demowebservice.entity.Users;

@Repository
public interface UsersRespository extends JpaRepository<Users, Integer> {
	@Query("SELECT u from Users u where u.username = :username")
	List<Users> findByUsername(@Param("username") String username);
	@Query("SELECT u from Users u where u.username = :username AND u.password = :password")
	List<Users> findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}