package com.example.demowebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demowebservice.service.UsersService;

@RestController
@CrossOrigin
public class UsersController {
	
	@Autowired
	private UsersService usersService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<?> getUsers() throws Exception {
		return ResponseEntity.ok(usersService.getUsers());
	}
}
