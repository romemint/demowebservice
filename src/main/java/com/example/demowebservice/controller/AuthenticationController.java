package com.example.demowebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demowebservice.service.AuthenticationService;
import com.example.demowebservice.config.JwtTokenUtil;
import com.example.demowebservice.model.JwtRequest;
import com.example.demowebservice.model.JwtResponse;
import com.example.demowebservice.model.RegisterRequest;
import com.example.demowebservice.model.RegisterResponse;

@RestController
@CrossOrigin
public class AuthenticationController {
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	private AuthenticationService authenticationService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody RegisterRequest registerRequest) throws Exception {
		RegisterResponse registerResponse = authenticationService.register(registerRequest);
		return ResponseEntity.ok(registerResponse);
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

        System.out.println("getPasswordgetPasswordgetPassword"+authenticationRequest.getPassword());
		final UserDetails userDetails = authenticationService.loadUserByUsernameAndPassword(authenticationRequest);
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JwtResponse(token));
	}
}
