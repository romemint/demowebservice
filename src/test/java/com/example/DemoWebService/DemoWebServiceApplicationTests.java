package com.example.DemoWebService;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.util.concurrent.ThreadLocalRandom;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.demowebservice.config.JwtTokenUtil;
import com.example.demowebservice.model.RegisterRequest;
import com.example.demowebservice.model.RegisterResponse;
import com.example.demowebservice.service.impl.AuthenticationServiceImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoWebServiceApplicationTests {

	@LocalServerPort
	private int port;

	@InjectMocks
	private AuthenticationServiceImpl authenticationService;

	@Mock
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private TestRestTemplate restTemplate;

	@Before(value = "")
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@DisplayName("Integration test register of the Service")
	void registerTest() throws Exception {
		String baseUrl = "http://localhost:" + port;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		RegisterRequest registerRequest = new RegisterRequest();
		ThreadLocalRandom current = ThreadLocalRandom.current();
		int randomValue = current.nextInt(55555);
		registerRequest.setUsername("test" + randomValue);
		registerRequest.setPassword("test1");
		registerRequest.setAddress("address");
		registerRequest.setPhone("0882993322");
		registerRequest.setSalary(55555d);
		HttpEntity<RegisterRequest> request = new HttpEntity<>(registerRequest, headers);
		ResponseEntity<RegisterResponse> result = this.restTemplate.postForEntity(new URI(baseUrl + "/register"),
				request, RegisterResponse.class);
		assertEquals("success", result.getBody().getStatus());

		randomValue = current.nextInt(101);
		registerRequest.setUsername("test" + randomValue);
		registerRequest.setPassword("test2");
		registerRequest.setAddress("address");
		registerRequest.setPhone("0882993344");
		registerRequest.setSalary(55555d);
		request = new HttpEntity<>(registerRequest, headers);
		result = this.restTemplate.postForEntity(new URI(baseUrl + "/register"), request, RegisterResponse.class);
		assertEquals("success", result.getBody().getStatus());

		randomValue = current.nextInt(101);
		registerRequest.setUsername("test" + randomValue);
		registerRequest.setPassword("test3");
		registerRequest.setAddress("address");
		registerRequest.setPhone("0882993355");
		registerRequest.setSalary(55555d);
		request = new HttpEntity<>(registerRequest, headers);
		result = this.restTemplate.postForEntity(new URI(baseUrl + "/register"), request, RegisterResponse.class);
		assertEquals("success", result.getBody().getStatus());
	}

	@Test
	@DisplayName("Integration test genReferenceCode of the Service")
	void loginAndGetUsersTest() throws Exception {
		String referenceCode = authenticationService.genReferenceCode("332255");
		assertEquals(12, referenceCode.length());

	}

}
